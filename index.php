<?php

namespace Asian\WPRedis;

include "Config.php";
include "AjaxHandler.php";

Config::init();
AjaxHandler::getInstance()->run();

?>
