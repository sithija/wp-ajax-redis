<?php
/**
 * Config class
 *
 * @Package namespace Asian\WPRedis
 * @author Sithija
 */

namespace Asian\WPRedis;

class Config {

	/**
	 * Redis Config
	 */
	const REDIS_HOST = "127.0.0.1";
	const REDIS_PORT = 6379;
	const DEFAULT_TTL = 0; // TTL - Time To Life, 0 = persist, > 0 expired in x seconds
	const UNIQUE_KEY_PREFIX = "pocketimes:m2r:"; //Leave it blank if don't want any prefix
	const SYNC_INTERVAL = 600; //10 minutes

	const KEY_COUNT = "count:%d";
	const HASH_KEY_VIEWS = "views";
	const HASH_KEY_LIKES = "likes";
	const HASH_KEY_DISLIKES = "dislikes";

	const KEY_COUNT_TRACKING = "count_tracking:%s:%d:%s";
	const KEY_LIKES = "likes";
	const KEY_DISLIKES = "dislikes";
	const KEY_REGISTERED = "registered";
	const KEY_UNREGISTERED = "unregistered";
	const KEY_COUNT_CHANGES = "count_changes:%d";

	const UNREGISTERED_COOKIE = "asian_pi";
	const REGISTERED_COOKIE = "asian_user_id";

	/**
	 * initialization
	 */
	public static function init() {
		date_default_timezone_set("Asia/Singapore");
	}
}

?>
