<?php
/**
 * Config class
 *
 * @Package namespace Asian\WPRedis
 * @author Sithija
 */

namespace Asian\WPRedis;

class AjaxHandler {
	protected static $instance = null;
	private $redis;
	private $post_id;

	public static function getInstance($params = null) {
		if(is_null(self::$instance)) {
			self::$instance = new AjaxHandler();
		}
		return self::$instance;
	}

	function __construct() {}

	public function run(){

		if (isset($_POST['action'])) {
			$action = $_POST['action'];
		} else {
			echo "Invalid request:No Action";
		    exit();
		}

		/**
		 * Allow only selected ajax requests that we know.
		 * @var array
		 */
		$actionList = array('td_ajax_update_views', 'get_video_like_dislike_count', 'save_video_likes');


		/**
		 * We process only allowed ajax calls
		 */
		if ( !in_array( $action, $actionList ) ) {
		    echo "Invalid request:Action not allowed";
		    exit();
		}

		$this->redis = new \Redis();
		$this->redis->connect(Config::REDIS_HOST, Config::REDIS_PORT);

		switch ($action) {
		    case 'td_ajax_update_views':
		        $this->update_n_get_view_count();
		        break;
		    case 'get_video_like_dislike_count':
		    	$this->get_video_like_dislike_count();
		    	break;
		    case 'save_video_likes':
		        $this->save_video_likes();
		        break;
		    case 'save_video_favorite':
		        $this->save_video_favorite();
		        break;
		}
	}

	private function update_n_get_view_count(){

		if (isset($_POST['td_post_ids'])) {
			$posts = $_POST['td_post_ids'];
		} else {
			echo "Invalid request:No post ID";
		    exit();
		}
		/**
		 * This request sends an array of post IDs.
		 * We deal only with first post ID.
		 * @var [type]
		 */
		$posts = json_decode($posts);
		$post = $posts[0];

		/**
		 * Increase view count by 1 and get the result.
		 */
		$hashKey = Config::UNIQUE_KEY_PREFIX . sprintf(Config::KEY_COUNT, $post);
		$value = $this->redis->hIncrBy($hashKey, 'views', 1);

		/**
		 * List the updated posts
		 */
		$now = time();
		$timestamp = $now - ($now % Config::SYNC_INTERVAL);
		$setsKey = Config::UNIQUE_KEY_PREFIX . sprintf(Config::KEY_COUNT_CHANGES, $timestamp);
		$this->redis->sAdd($setsKey, $post);

		echo '{"'.$post.'":'.$value.'}';
	}

	private function get_video_like_dislike_count($user_status = false){
		if (isset($_POST['post_id'])) {
			$post = $_POST['post_id'];
		} else {
			echo "Invalid request:No post ID";
		    exit();
		}

		$hashKey = Config::UNIQUE_KEY_PREFIX . sprintf(Config::KEY_COUNT, $post);
		$likes = $this->redis->hGet($hashKey, 'likes');
		$dislikes = $this->redis->hGet($hashKey, 'dislikes');
		if (!$likes) {
			$likes = 0;
		}
		if (!$dislikes) {
			$dislikes = 0;
		}

		if (!$user_status) {
			$user_status = $this->hasLikedDisliked($post);
		}

		echo "$likes,$dislikes,$user_status";
	}

	private function save_video_likes(){

		if (isset($_POST['post_slug']) && isset($_POST['vote']) && isset($_POST['post_id'])) {
			$post_slug = $_POST['post_slug'];
			$postId = $_POST['post_id'];
			$vote = $_POST['vote'];
		} else {
			echo "Invalid request:Missing post data";
		    exit();
		}

		/**
		 * Check if user has given a like or a dislike.
		 * If vote=up and user has liked before, no change is needed.
		 * If vote=up and user has disliked before, we remove dislike and decrease dislike count by 1.
		 * 		Then add like and increase like count by 1.
		 * If vote=down and user has disliked before, no change is needed.
		 * If vote=down and user has liked before, we remove like and decrease like count by 1.
		 * 		Then add dislike and increase dislike count by 1.
		 */
		/**
		 * First check if user has already given a like/dislike
		 */
		$unregisteredLikesKey 	 = Config::UNIQUE_KEY_PREFIX . sprintf(Config::KEY_COUNT_TRACKING, Config::HASH_KEY_LIKES, $postId, Config::KEY_UNREGISTERED);
		$unregisteredDislikesKey = Config::UNIQUE_KEY_PREFIX . sprintf(Config::KEY_COUNT_TRACKING, Config::HASH_KEY_DISLIKES, $postId, Config::KEY_UNREGISTERED);
		$registeredLikesKey 	 = Config::UNIQUE_KEY_PREFIX . sprintf(Config::KEY_COUNT_TRACKING, Config::HASH_KEY_LIKES, $postId, Config::KEY_REGISTERED);
		$registeredDislikesKey 	 = Config::UNIQUE_KEY_PREFIX . sprintf(Config::KEY_COUNT_TRACKING, Config::HASH_KEY_DISLIKES, $postId, Config::KEY_REGISTERED);

		$result = $this->getUserInfo($postId);

		$user_ID = $result["user_ID"];
		$user_type = $result["user_type"];
		$user_found = $result["user_found"];
		$action_type = $result["action_type"];

		/**
		 * If vote=up and user has liked before, no change is needed.
		 * If vote=down and user has disliked before, no change is needed.
		 */
		if ($action_type) {
			if (
				($vote == 'up' && $action_type == Config::KEY_UNREGISTERED . ':' . Config::HASH_KEY_LIKES) ||
				($vote == 'up' && $action_type == Config::KEY_REGISTERED . ':' . Config::HASH_KEY_LIKES) ||
				($vote == 'down' && $action_type == Config::KEY_UNREGISTERED . ':' . Config::HASH_KEY_DISLIKES) ||
				($vote == 'down' && $action_type == Config::KEY_REGISTERED . ':' . Config::HASH_KEY_DISLIKES)
				) {
				echo "No action needed";
				return;
			}
		}

		/**
		 * Update SortedSets and Hash with total count.
		 * If $user_found == true we must delete that key and reduce total count.
		 */
		if ($user_found) {
			$this->remove_like_dislike($postId, $user_ID, $vote, $action_type, $unregisteredLikesKey, $unregisteredDislikesKey, $registeredLikesKey, $registeredDislikesKey);
		}

		$this->add_like_dislike($postId, $user_ID, $vote, $user_type, $unregisteredLikesKey, $unregisteredDislikesKey, $registeredLikesKey, $registeredDislikesKey);

		/**
		 * List the updated posts
		 */
		$now = time();
		$timestamp = $now - ($now % Config::SYNC_INTERVAL);
		$setsKey = Config::UNIQUE_KEY_PREFIX . sprintf(Config::KEY_COUNT_CHANGES, $timestamp);
		$this->redis->sAdd($setsKey, $postId);

		/**
		 * Now we return the updated like/dislike count.
		 */
		$user_status = ($vote == 'up') ? 1 : -1;
		$this->get_video_like_dislike_count($user_status);
	}

	private function remove_like_dislike($postId, $user_ID, $vote, $action_type, $unregisteredLikesKey, $unregisteredDislikesKey, $registeredLikesKey, $registeredDislikesKey){
		/**
		 * Remove sorted set
		 */
		switch ($action_type) {
		    case Config::KEY_UNREGISTERED . ':' . Config::HASH_KEY_LIKES:
		        $this->redis->zRem($unregisteredLikesKey, $user_ID);
		        break;
		    case Config::KEY_UNREGISTERED . ':' . Config::HASH_KEY_DISLIKES:
		        $this->redis->zRem($unregisteredDislikesKey, $user_ID);
		        break;
		    case Config::KEY_REGISTERED . ':' . Config::HASH_KEY_LIKES:
		        $this->redis->zRem($registeredLikesKey, $user_ID);
		        break;
		    case Config::KEY_REGISTERED . ':' . Config::HASH_KEY_DISLIKES:
		        $this->redis->zRem($registeredDislikesKey, $user_ID);
		        break;
		}
		
		/**
		 * Decrease total count by one
		 */
		$hashKey = Config::UNIQUE_KEY_PREFIX . sprintf(Config::KEY_COUNT, $postId);
		if ($vote == 'up') {
			$this->redis->hIncrBy($hashKey, 'dislikes', -1);
		} else {
			$this->redis->hIncrBy($hashKey, 'likes', -1);
		}
	}

	private function add_like_dislike($postId, $user_ID, $vote, $user_type, $unregisteredLikesKey, $unregisteredDislikesKey, $registeredLikesKey, $registeredDislikesKey){
		/**
		 * Add sorted set
		 */
		if ($user_type == Config::KEY_UNREGISTERED) {
			if ($vote == 'up') {
				$this->redis->zAdd($unregisteredLikesKey, time(), $user_ID);
			} else {
				$this->redis->zAdd($unregisteredDislikesKey, time(), $user_ID);
			}
		} elseif ($user_type == Config::KEY_REGISTERED) {
			if ($vote == 'up') {
				$this->redis->zAdd($registeredLikesKey, time(), $user_ID);
			} else {
				$this->redis->zAdd($registeredDislikesKey, time(), $user_ID);
			}
		}
		/**
		 * Increase total count by one
		 */
		$hashKey = Config::UNIQUE_KEY_PREFIX . sprintf(Config::KEY_COUNT, $postId);
		if ($vote == 'up') {
			$this->redis->hIncrBy($hashKey, 'likes', 1);
		} else {
			$this->redis->hIncrBy($hashKey, 'dislikes', 1);
		}
	}

	private function hasLikedDisliked($postId){
		$result = $this->getUserInfo($postId);

		$user_type = $result["user_type"];
		$user_found = $result["user_found"];
		$action_type = $result["action_type"];

		if (!$user_found) {
			return 0;
		}

		switch ($action_type) {
		    case Config::KEY_UNREGISTERED . ':' . Config::HASH_KEY_LIKES:
		        return 1;
		        break;
		    case Config::KEY_UNREGISTERED . ':' . Config::HASH_KEY_DISLIKES:
		        return -1;
		        break;
		    case Config::KEY_REGISTERED . ':' . Config::HASH_KEY_LIKES:
		        return 1;
		        break;
		    case Config::KEY_REGISTERED . ':' . Config::HASH_KEY_DISLIKES:
		        return -1;
		        break;
		}
	}

	private function getUserInfo($postId){
		$user_ID = 0;
		$user_type = '';
		$user_found = false;
		$action_type = false;
		$unregisteredLikesKey 	 = Config::UNIQUE_KEY_PREFIX . sprintf(Config::KEY_COUNT_TRACKING, Config::HASH_KEY_LIKES, $postId, Config::KEY_UNREGISTERED);
		$unregisteredDislikesKey = Config::UNIQUE_KEY_PREFIX . sprintf(Config::KEY_COUNT_TRACKING, Config::HASH_KEY_DISLIKES, $postId, Config::KEY_UNREGISTERED);
		$registeredLikesKey 	 = Config::UNIQUE_KEY_PREFIX . sprintf(Config::KEY_COUNT_TRACKING, Config::HASH_KEY_LIKES, $postId, Config::KEY_REGISTERED);
		$registeredDislikesKey 	 = Config::UNIQUE_KEY_PREFIX . sprintf(Config::KEY_COUNT_TRACKING, Config::HASH_KEY_DISLIKES, $postId, Config::KEY_REGISTERED);

		if (isset($_COOKIE[Config::REGISTERED_COOKIE])) {
			$user_ID = $_COOKIE[Config::REGISTERED_COOKIE];
		}

		if (isset($user_ID) && $user_ID != 0) {
			$user_type = Config::KEY_REGISTERED;
		} else {
			$user_type = Config::KEY_UNREGISTERED;

			if (isset($_COOKIE[Config::UNREGISTERED_COOKIE])) {
				$user_ID = $_COOKIE[Config::UNREGISTERED_COOKIE];
			} else {
				echo "Invalid request:Cookie not found";
		    	exit();
			}
		}


		if ($user_type == Config::KEY_UNREGISTERED) {
			$rank = $this->redis->zRank($unregisteredLikesKey, $user_ID);

			if ($rank === false) {
				$rank = $this->redis->zRank($unregisteredDislikesKey, $user_ID);
				if ($rank !== false) {
					$user_found = true;
					$action_type = Config::KEY_UNREGISTERED . ':' . Config::HASH_KEY_DISLIKES;
				}
			} else {
				$user_found = true;
				$action_type = Config::KEY_UNREGISTERED . ':' . Config::HASH_KEY_LIKES;
			}
		} elseif ($user_type == Config::KEY_REGISTERED) {
			$rank = $this->redis->zRank($registeredLikesKey, $user_ID);

			if ($rank === false) {
				$rank = $this->redis->zRank($registeredDislikesKey, $user_ID);
				if ($rank !== false) {
					$user_found = true;
					$action_type = Config::KEY_REGISTERED . ':' . Config::HASH_KEY_DISLIKES;
				}
			} else {
				$user_found = true;
				$action_type = Config::KEY_REGISTERED . ':' . Config::HASH_KEY_LIKES;
			}
		}

		return array(
			'user_ID' => $user_ID,
			'user_type' => $user_type,
			'user_found' => $user_found,
			'action_type' => $action_type
		);
	}
}
?>